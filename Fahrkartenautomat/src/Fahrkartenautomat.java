﻿import java.util.Scanner;

class Fahrkartenautomat {

	// Fahrkarten erfassen
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
	    byte tickettyp = 0; 
	    byte ticketanzahl;
	    double zuZahlenderBetrag = 0;
	    boolean neuerFahrschein = true;
	    int schleifenZaehler = 0;
	    double zwischensumme = 0;
	    
	    while (neuerFahrschein == true) {
		    if (schleifenZaehler == 0) {
			    System.out.print("Was möchten sie kaufen?\n" + "Einzelfahrausweis (1)\n" + "Tageskarte (2)\n" + "Gruppenkarte (3)\n");
			    tickettyp = tastatur.nextByte();
			    while (!((tickettyp == 1) || (tickettyp == 2) || (tickettyp == 3))) {
			    	System.out.println("Bitte gültiges Ticket auswählen!");
			    	tickettyp = tastatur.nextByte();
			    }
			    
			    System.out.print("Anzahl der Tickets eingeben: ");
			    ticketanzahl = tastatur.nextByte();
			    while (ticketanzahl>10 || ticketanzahl<1) {
			    	System.out.println("Die Ticketanzahl muss zwischen 1 und 10 liegen. Bitte erneut eingeben!");
			    	ticketanzahl = tastatur.nextByte();
			    }
			    
			    if (tickettyp == 1)	{
			    	zwischensumme = ticketanzahl * 2.9;
			    }
		
			    if (tickettyp == 2) {
			    	zwischensumme = ticketanzahl * 8.6;
			    }
		
			    if (tickettyp == 3) {
			    	zwischensumme = ticketanzahl * 23.5;
			    }	
		    }
		    
		    schleifenZaehler++;
		    
		    if (schleifenZaehler > 1) {
			    System.out.print("\nWas möchten sie als nächstes tun? \n" + "Einzelfahrausweis hinzufügen (1)\n" + "Tageskarte hinzufügen (2)\n" + "Gruppenkarte hinzufügen (3)\n" + "Bezahlen (9)\n");
			    tickettyp = tastatur.nextByte();
			    while (!((tickettyp == 1) || (tickettyp == 2) || (tickettyp == 3) || (tickettyp == 9))) {
			    	System.out.println("Bitte gültiges Eingabe machen!");
			    	tickettyp = tastatur.nextByte();
			    }
			    if (tickettyp !=9) {
				    System.out.print("Anzahl der Tickets: ");
				    ticketanzahl = tastatur.nextByte();
				    while (ticketanzahl>10 || ticketanzahl<1) {
				    	System.out.println("Die Ticketanzahl muss zwischen 1 und 10 liegen. Bitte erneut eingeben!");
				    	ticketanzahl = tastatur.nextByte();
				    }
			
				    if (tickettyp == 1)	{
				    	zwischensumme = zwischensumme + (ticketanzahl * 2.9);
				    }
			
				    if (tickettyp == 2) {
				    	zwischensumme = zwischensumme + (ticketanzahl * 8.6);
				    }
			
				    if (tickettyp == 3) {
				    	zwischensumme = zwischensumme + (ticketanzahl * 23.5);
				    }
			    }
			    
			    if (tickettyp == 9) {
			    	zuZahlenderBetrag = zwischensumme;
			    	neuerFahrschein = false;
			    }
			 }
	    }
		return zuZahlenderBetrag;
	}
	    
	
	// Fahrkarten bezahlen
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur2 = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("%s %.2f %s \n ", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"EURO");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMuenze = tastatur2.nextDouble();
			while (((100*eingeworfeneMuenze) % 5 !=0) || eingeworfeneMuenze>2 || eingeworfeneMuenze<0) {
				System.out.println("Bitte zulässige Münze einwerfen!");
				eingeworfeneMuenze = tastatur2.nextDouble();
			}
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
		
	}
	
	// Wartezeit
	public static void warte(int warteZeit) {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(warteZeit);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	public static void rueckgeldausgabe(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = 0.0;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag + 0.001;
		if (rückgabebetrag > 0.0) {
			System.out.printf("%s %.2f %s \n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, "EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			
			muenzeAusgeben(rückgabebetrag, "");
		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+"vor Fahrtantritt entwerten zu lassen!\n"+"Wir wünschen Ihnen eine gute Fahrt.\n");
		System.out.println("Nächster Fahrschein kannn bestellt werden\n");
	}
	
	public static void muenzeAusgeben(double rückgabebetrag, String einheit) {
		while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			System.out.println("2 EURO");
			rückgabebetrag -= 2.0;
		}
		while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			System.out.println("1 EURO");
			rückgabebetrag -= 1.0;
		}
		while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			System.out.println("50 CENT");
			rückgabebetrag -= 0.5;
		}
		while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			System.out.println("20 CENT");
			rückgabebetrag -= 0.2;
		}
		while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
		{
			System.out.println("10 CENT");
			rückgabebetrag -= 0.1;
		}
		while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
		{
			System.out.println("5 CENT");
			rückgabebetrag -= 0.05;
		}
	}
	
	public static void main(String[] args) {

			double zuZahlenderBetrag = 0;
			double eingezahlterGesamtbetrag = 0.0;
			boolean neustarter = true;
			
			while (neustarter == true) {
				// Fahrkarenbestellung erfassen
				zuZahlenderBetrag = fahrkartenbestellungErfassen();

				// Geldeinwurf
				// -----------
				eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

				// Fahrscheinausgabe
				// -----------------
				warte(250);

				// Rückgeldberechnung und -Ausgabe
				// -------------------------------
				rueckgeldausgabe(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			}
	}
}
