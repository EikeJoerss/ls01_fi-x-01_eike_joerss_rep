import java.util.Scanner;

public class Ohm {

	public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Welche Gr��e m�chten Sie berechnen? (r, u, i)");
			char eingabe = sc.next().charAt(0);
			
			switch (eingabe) {
			
			case 'r':
				System.out.println("Geben sie die Wert f�r u in Volt ein!");
				double u = sc.nextDouble();
				System.out.println("Geben sie die Wert f�r i in Ampere ein!");
				double i = sc.nextDouble();
				double r = u/i;
				System.out.printf("Das Ergebnis lautet: %.2f Ohm", r);
				break;
				
			case 'i':
				System.out.println("Geben sie die Wert f�r u in Volt ein!");
				double u_2 = sc.nextDouble();
				System.out.println("Geben sie die Wert f�r r in Ohm ein!");
				double r_2 = sc.nextDouble();
				double i_2 = u_2/r_2;
				System.out.printf("Das Ergebnis lautet: %.2f Ampere", i_2);
				break;
				
			case 'u':
				System.out.println("Geben sie die Wert f�r i in Ampere ein!");
				double i_3 = sc.nextDouble();
				System.out.println("Geben sie die Wert f�r r in Ohm ein!");
				double r_3 = sc.nextDouble();
				double u_3=r_3*i_3;
				System.out.printf("Das Ergebnis lautet: %.2f Volt", u_3);
				break;
				
			default:
				System.out.println("Bitte g�ltige Gr��e eingeben");
			
			if (sc != null)
				sc.close();
			}
		}

	

}
