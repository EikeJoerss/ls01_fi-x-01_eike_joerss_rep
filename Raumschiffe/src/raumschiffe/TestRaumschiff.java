package raumschiffe;

public class TestRaumschiff {

	public static void main(String[] args) {
		
		//Raumschiff initialisieren
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2,"IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var");
		
		//Ladungen initialisieren
		Ladung schneckensaft = new Ladung ("Ferengi Schneckensaft",200);
		Ladung schrott = new Ladung ("Borg-Schrott",5);
		Ladung materie = new Ladung ("Rote Materie",2);
		Ladung sonde = new Ladung ("Forschungssonde",35);
		Ladung schwert = new Ladung ("Bat'leth Klingonen Schwert",200);
		Ladung plasma = new Ladung ("Plasma Waffe",50);
		Ladung photonentorpedos = new Ladung ("Photonentorpedo",3);
		
		//Ladungen zuweisen
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);
		romulaner.addLadung(materie);
		romulaner.addLadung(schrott);
		romulaner.addLadung(plasma);
		vulkanier.addLadung(photonentorpedos);
		vulkanier.addLadung(sonde);
		
		//Die Schlacht beginnt
		//-------------------------------------------
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(photonentorpedos.getMenge(),photonentorpedos);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		vulkanier.eintraegeLogbuchZurueckgeben();
		//Die Schlacht endet
		//-------------------------------------------
	}
}
