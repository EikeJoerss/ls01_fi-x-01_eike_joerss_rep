package raumschiffe;

/**
 * @author Eike
 * @version 0.1
 * Diese Klasse definiert die Ladungen der Raumschiffe
 * jeder Ladung ist dabei eine Bezeichnung und eine Menge zugeordnet
 */
public class Ladung {
	
	/** Bezeichnung der Ladung */
	private String bezeichnung;
	/** Menge der Ladung */
	private int menge;
	
	/**
	 * parameterloser Konstruktor für das Objekt Ladung
	 */
	public Ladung() {
		
	}
	
	/**
	 * vollständiger Konstruktor für das Objekt Ladung
	 * @param bezeichnung
	 * Bezeichnung der Ladung
	 * @param menge
	 * Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/**
	 * legt die Bezeichnung der Ladung fest
	 * @param bezeichnung
	 * Bezeichnung der Ladung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * gibt die Bezeichnung der Ladung zurück
	 * @return bezeichnung
	 */
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	/**
	 * legt die Menge der Ladung fest
	 * @param menge
	 * Menge der Ladung
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	/**
	 * gibt die Menge der Ladung zurück
	 * @return menge
	 */
	public int getMenge() {
		return this.menge;
	}
	
}
