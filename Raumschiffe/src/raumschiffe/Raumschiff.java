package raumschiffe;

import java.util.ArrayList;

/**
 * @author Eike
 * @version 0.1
 * Diese Klasse definiert ein Raumschiff
 */
public class Raumschiff {
	
	//Atribute der Klasse Raumschiff
	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 * parameterloser Konstruktor für das Objekt Raumschiff
	 */
	public Raumschiff() {
		
	}
	
	/**
	 * vollständiger Konstruktor für das Objekt Raumschiff
	 * @param photonentorpedoAnzahl
	 * Anzahl der geladenden Torpedos
	 * @param energieversorgungInProzent
	 * Status der Energieversorgung in Prozent
	 * @param zustandSchildeInProzent
	 * Status der Schutzschilde in Prozent
	 * @param zustandHuelleInProzent
	 * Status der Schutzhuelle in Prozent
	 * @param zustandLebenserhaltungssystemeInProzent
	 * Status der Lebenserhaltungssysteme in Prozent
	 * @param anzahlDroiden
	 * Anzahl der Reparaturdroiden auf dem Schiff
	 * @param schiffsname
	 * Name des Raumschiffs
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden, String schiffsname) {
		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
	}
	
	public int getPhotonentorpedoAnzahl(){
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}
	
	public int getLebenserhaltungsystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Dem Ladungsverzeichnis wird eine Ladung hinzugefügt
	 * @param neueLadung
	 * Das Objekt der Klasse Ladung welches dem Ladungsverzeichnis hinzugefügt wird
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * gibt sämtliche Statuswerte des Raumschiffs in der Konsole aus
	 */
	public void zustandRaumschiff() {
		System.out.println("Schiffsname: " + schiffsname);
		System.out.println("Anzahl Photonentorpedos: " + photonentorpedoAnzahl);
		System.out.println("Energieversorgung in Prozent: " + energieversorgungInProzent);
		System.out.println("Schilde in Prozent: " + schildeInProzent);
		System.out.println("Huelle in Prozent: " + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + lebenserhaltungssystemeInProzent);
		System.out.println("Anzahl Androiden: " + androidenAnzahl);
	}
	
	/**
	 * gibt alle Ladungen des Raumschiffs mit Bezeichnung und Menge in der Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung.getBezeichnung() + ": " + ladung.getMenge());
		}
	}
	
	/**
	 * Schießt einen Photonentorpedo auf ein feindliches Raumschiff
	 * Sind keine Torpedos geladen wird CLICK an alle Ausgegeben
	 * Ist mindestens ein Torpedos vorhanden wird ein Treffer registriert, die Anzahl der geladenen Torpedos um 1 reduziert und "Photonentorpedo abgeschossen an alle ausgegeben"
	 * @param r
	 * feindliches Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
				
		if(r.getPhotonentorpedoAnzahl() <= 0) {
			r.nachrichtAnAlle("-=*Click*=-");
		}
		
		else {
			r.setPhotonentorpedoAnzahl(r.getPhotonentorpedoAnzahl()-1);
			r.nachrichtAnAlle("Photonentorpedo abgeschossen");
			r.treffer(r);
		}
	}
	
	/**
	 * Sendet eine Nachricht an alle Schiffe
	 * Schreibt einen String in das statische Attribut broadcastKommunikator
	 * @param message
	 * Nachricht, die an alle gesendet wird
	 */
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	
	/**
	 * registriert einen Treffer des Schiffs, sendet "wurde getroffen" Nachricht an alle, reduziert Schilde des getroffenen Schiffs um 50%, wenn die Schilde unter 50% fallen werden Energie und Huelle ebenfalls um 50% reduziert, fällt die Huelle auf 0 werden die Lebenerhaltungssysteme auf 0 reduziert und das an alle gesendet
	 * @param r
	 * feindliches Raumschiff
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
		
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		
		if(r.getSchildeInProzent() <= 0) {
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
			
			if(r.getHuelleInProzent() <= 0) {
				r.setLebenserhaltungssystemeInProzent(0);
				r.nachrichtAnAlle(getSchiffsname() + " Lebenserhaltungssysteme vernichtet");
			}
		}
	}
	
	/**
	 * Schießt die Phaserkanone ab
	 * ist die Energieversorgung des einsetzenden Schiffs unter 50% wird nicht geschossen, CLICK wird an alle gesendet
	 * ist die Energie des einsetzenden Schiff größer als 50% wird diese im 50% reduziert und am feindlich Schiff ein Treffer registriert
	 * Phaserkanone abgeschossen wird an alle gesendet
	 * @param r
	 * feindliches Schiff
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		
		if(this.getEnergieversorgungInProzent() < 50) {
			this.nachrichtAnAlle("-=*Click*=-");
		}
		
		else {
			this.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			r.treffer(r);
		}
	}
	
	/**
	 * gibt alles Einträge des broadcastKommunikator in der Konsole aus
	 */
	public void eintraegeLogbuchZurueckgeben() {
		for(String eintraege : broadcastKommunikator) {
		     System.out.println(eintraege);
		}
		
	}
	
	/**
	 * Lädt ind Ladung vorhandene Torpedos in die Torpedosrohre
	 * sind keine Torpedos geladen wird keine Torpedos gefunden in der Konsole ausgegeben und CLICK an alle gesendet
	 * ansosnten wird entsprechend der zu ladenden Anzahl die Anzahl der geladenen Torpedos erhöht und die Anzahl der im Laderaum befindlichen Torpedos reduziert
	 * @param zuLadendeTorpedos
	 * Anzahl der zu ladenden Torpedos
	 * @param photonentorpedos
	 * Objekt der Klasse Ladung in dem die Menge der im Laderaum befindlichen Torpedos gespeichert ist
	 */
	public void photonentorpedosLaden(int zuLadendeTorpedos, Ladung photonentorpedos) {
		
		int anzahlTorpedosInLadung = photonentorpedos.getMenge();
		
		if(anzahlTorpedosInLadung == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
		
		if(zuLadendeTorpedos > anzahlTorpedosInLadung) {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedosInLadung);
			photonentorpedos.setMenge(0);
			System.out.println(getPhotonentorpedoAnzahl() + " Photonentorpedo(s) geladen");
		}
		
		else if(zuLadendeTorpedos <= anzahlTorpedosInLadung) {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + zuLadendeTorpedos);
			photonentorpedos.setMenge(photonentorpedos.getMenge() - zuLadendeTorpedos);
			System.out.println(getPhotonentorpedoAnzahl() + " Photonentorpedo(s) geladen");
		}
	}
	
	/**
	 * löscht alle Einträge aus der Liste ladungsverzeichnis, bei den die Menge 0 ist
	 */
	public void ladungsverzeichnisAufraeumen(){
		for(Ladung ladung : ladungsverzeichnis) {
			if(ladung.getMenge() <= 0) {
				ladungsverzeichnis.remove(ladung);
			}
		}
	}
	
	/**
	 * Repariert die angebenen Systeme
	 * Der Reparaturwert hängt von der Anzahl der eingesetzten Droiden und einer Zufallszahl ab.
	 * Die Berechnung erfolgt mittels der in der Aufgabenstellung angegeben Formel
	 * @param schutzschilde
	 * gibt an ob die Schutschile repariert werden sollen
	 * @param energieversorgung
	 * gibt an ob die Energieversorgung repariert werden soll
	 * @param schiffshuelle
	 * gibt an ob die Huelle repariert werden soll
	 * @param
	 * legt die Anzahl der einzusetzenden Droiden fest
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
		int zufallszahl = (int) (Math.random() * 100);
		int anzahlDroidenImEinsatz;
		int anzahlSchiffsstrukturen = 0;
		int reparaturwert = 0;
		
		if(anzahlDroiden > getAndroidenAnzahl()) {
			anzahlDroidenImEinsatz = getAndroidenAnzahl();
		}
		
		else {
			anzahlDroidenImEinsatz = anzahlDroiden;
		}
		
		if(schutzschilde) {
			anzahlSchiffsstrukturen++;
		}
		
		if(energieversorgung) {
			anzahlSchiffsstrukturen++;
		}
		
		if(schiffshuelle) {
			anzahlSchiffsstrukturen++;
		}
		
		if(anzahlSchiffsstrukturen > 0) {
			reparaturwert = (zufallszahl * anzahlDroidenImEinsatz) / anzahlSchiffsstrukturen;
		}
		
		if(schutzschilde) {
			setSchildeInProzent(getSchildeInProzent() + reparaturwert);
			
			if(getSchildeInProzent()>100) {
				setSchildeInProzent(100);
			}
		}
		
		if(energieversorgung){
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reparaturwert);
			
			if(getEnergieversorgungInProzent()>100) {
				setEnergieversorgungInProzent(100);
			}
		}
		
		if(schiffshuelle) {
			setHuelleInProzent(getHuelleInProzent() + reparaturwert);
			
			if(getHuelleInProzent()>100) {
				setHuelleInProzent(100);
			}
		}
	}
}
