import java.util.Scanner; //1
public class mittelwert {

	public static double verarbeitung(double pZahl1, double pZahl2) {
		double ergebnis;
		ergebnis = (pZahl1+pZahl2)/2;
		return ergebnis;
	}
	
   public static void main(String[] args) {

	   
	   
	  try (Scanner meinScanner = new Scanner(System.in)) {
		// Deklaration der Variablen
		  double zahl1;
		  double zahl2;
		  double mittelwert;
		  
		  // (E) "Eingabe"
		  
		  System.out.print("Wie lautet der erste Wert: ");
		  zahl1 = meinScanner.nextDouble();
		  
		  System.out.print("Wie lautet der zweite Wert: ");
		  zahl2 = meinScanner.nextDouble();
		  
		  // (V) Verarbeitung

		  mittelwert=verarbeitung(zahl1,zahl2);
		  
		  // (A) Ausgabe
		  // Ergebnis auf der Konsole ausgeben:
		  // =================================
		  System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, mittelwert);
	}
   }
}
