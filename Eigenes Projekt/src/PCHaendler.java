import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		text = myScanner.next();
		return text;
	}

	public static int liesInt(String text){
		int anzahl = 0;
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text){
		double nettopreis = 0;
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		nettopreis = myScanner.nextDouble();
		return nettopreis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, " %");

	}
	public static void main(String[] args) {
		
		//Artikel einlesen
		String artikel = liesString("Was m�chten Sie bestellen?");
		
		//Anzahl einlesen
		int anzahl=liesInt("Geben Sie die Anzahl ein:");
		
		//Nettopreis einlesen
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		
		//Mehrwertsteuersatz einlesen
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		// Ausgeben der Rechnung
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

}