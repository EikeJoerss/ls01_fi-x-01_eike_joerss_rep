
public class Primzahlen {

	public static void main(String[] args) {
		
		for(int i = 2 ; i <= 100 ; i++) {
			boolean istprim = true;
			int a = i;
			
			for(int i_2 = 2 ; i_2<a/2 ; i_2++) {
				if(a%i_2 == 0) {
					istprim = false;
				}
			}
			if(istprim == true) {
				System.out.print(" " + a + " ");
			}
		}

	}

}
