import java.util.Scanner; 
public class Summe {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein:");
		int n = sc.nextInt();
		int a = 0;
		int b = 0;
		int c = 0;
		
		for(int i = 1; i <= n; i++) {
			a += i;
		}
		
		for(int i = 0; i <= n; i = i+2) {
			b += i;
		}
		
		for(int i = 1; i <= n; i = i+2) {
			c += i;
		}
		
		System.out.printf("Die Summe f�r A betr�gt: %d%n", a);
		System.out.printf("Die Summe f�r B betr�gt: %d%n", b);
		System.out.printf("Die Summe f�r C betr�gt: %d%n", c);
	}

}